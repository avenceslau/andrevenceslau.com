import React from "react";
import { navLinks } from "../utils/data";
import Link from "next/link";
import styles from '../styles/Home.module.css'

export default function Navbar() {
    return (
        <header>
            <nav className={styles.footer}>
                <ul>
                {navLinks.map(
                    (link, index) => {
                    return (
                            <Link key={index} href={link.path}>
                                <li>{link.name}</li>
                            </Link>
                    );
                })}
                </ul>
            </nav>
        </header>
    );
}