export const navLinks = [
    {
        name: "Home",
        path: "/"
    },
    {
        name: "About Me",
        path: "/about"
    },
    {
        name: "Contacts",
        path: "#contact"
    },
    {
        name: "Projects",
        path: "/projects"
    }
];