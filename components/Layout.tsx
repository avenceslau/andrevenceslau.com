import Navbar from "./navbar";

export const Layout = ({children}: any) => {
    return (
        <div className="content">
            <Navbar/>
            {children}
        </div>
    )
}